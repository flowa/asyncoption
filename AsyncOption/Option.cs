using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading.Tasks;

namespace AsyncOption
{
    public static class Option
    {
        public static Option<T> From<T>(T value)
        {
            return Option<T>.From(value);
        }

        public static Option<T> NoneOf<T>()
        {
            return Option<T>.AsNone();
        }
    }


    public class Option<T>
    {
        public class None : Option<T> { }
        public class Some : Option<T>
        {
            public Some(T obj)
            {
                this.Value = obj;
            }

            public T Value { get; }
        }

        public bool IsSome => this is Option<T>.Some;
        public bool IsNone => this is Option<T>.None;

        public Option<TResult> Map<TResult>(
            Func<T, Option<TResult>> mapSome,
            Func<Option<TResult>> mapNone = null
            ) => OptionMapper.Map<T, TResult>(this, mapSome, mapNone);

        public Option<TResult> FlatMap<TResult>(
            Func<T, TResult> mapSome,
            Func<TResult> mapNone = null
            ) => OptionMapper.FlatMap<T, TResult>(this, mapSome, mapNone);

        public async Task<Option<TResult>> MapAsync<TResult>(
            Func<T, Task<Option<TResult>>> mapSome,
            Func<Task<Option<TResult>>> mapNone = null
            ) => await OptionAsyncMapper.MapAsync<T, TResult>(this, mapSome, mapNone);

        public async Task<Option<TResult>> FlatMapAsync<TResult>(
            Func<T, Task<TResult>> mapSome,
            Func<Task<TResult>> mapNone = null
            ) => await OptionAsyncMapper.FlatMapAsync<T, TResult>(this, mapSome, mapNone);

        public T GetValueOrDefault(T defaultValue = default(T))
        {
            if (this.IsSome)
            {
                return ((Option<T>.Some) this).Value;
            }
            return defaultValue;
        }

        internal static Option<T> From(T value)
        {
            if (value == null) return new Option<T>.None();
            return new Option<T>.Some(value);
        }

        internal static Option<T> AsNone()
        {
            return new Option<T>.None();
        }

        public static Option<T> FromObject(object value)
        {
            if (value is T)
            {
                return Option<T>.From((T) value);
            }
            return Option<T>.AsNone();
        }        

        public static implicit operator Option<T>(T value)
        {
            if (value == null) return new Option<T>.None();
            return new Option<T>.Some(value);
        }

        public static explicit operator T(Option<T> v)
        {
            return v.GetValueOrDefault();
        }
    }
}