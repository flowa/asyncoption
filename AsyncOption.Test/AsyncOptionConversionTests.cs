using Xunit;

namespace AsyncOption.Test
{
    public class AsyncOptionConversionTests
    {
        [Fact]
        public void From_method_should_convert_null_to_None()
        {
            var s = Option.From<string>(null);
            Assert.IsAssignableFrom<Option<string>.None>(s);
        }

        [Fact]
        public void From_method_should_convert_nonnull_to_Some()
        {
            var s = Option.From("foo");
            Assert.IsAssignableFrom<Option<string>.Some>(s);
        }

        [Fact]
        public void Test_IsSome_and_IsNone_for_Some()
        {
            var s = Option.From<string>("foo");
            Assert.True(s.IsSome);
            Assert.False(s.IsNone);
        }

        [Fact]
        public void Test_IsSome_and_IsNone_for_None()
        {
            var s = Option.From<string>(null);
            Assert.True(s.IsNone);
            Assert.False(s.IsSome);
        }

        [Fact]
        public void Test_automatic_type_conversions()
        {
            var s = (Option<string>)"foo";
            Assert.Equal("foo", s.GetValueOrDefault());
            Assert.IsAssignableFrom<Option<string>.Some>(s);

            var s1 = (string)s;

            s = (Option<string>)(null as string);
            Assert.Equal(null, s.GetValueOrDefault());
            Assert.IsAssignableFrom<Option<string>.None>(s);
        }

    }
}