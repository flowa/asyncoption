using System;
using System.Threading.Tasks;
using Xunit;

namespace AsyncOption.Test
{
    public class OptionWhenExtensionsTests
    {

        [Fact]
        public void Using_for_Some_should_call_action()
        {
            var s = Option.From("foo");
            var i = 0;
            s.DoWhenSome((v) => { i = v.Length; });
            Assert.Equal(3, i);
        }

        [Fact]
        public void Using_with_null_action_should_do_nothing()
        {
            var s = Option.From("foo");
            s.DoWhenSome(null);

            // Test ok as function did not throw and value is same.
            Assert.Equal("foo", s.GetValueOrDefault());
        }

        [Fact]
        public void Using_for_None_should_not_call_action()
        {
            var s = Option.NoneOf<string>();
            s.DoWhenSome((v) => { throw new Exception("This should not happen"); });
            Assert.True(s.IsNone);
        }

        [Fact]
        public void UsingAsync_for_Some_should_call_action()
        {
            var s = Option.From("foo");
            var i = 0;
            var task = s.DoWhenSomeAsync(async (v) =>
            {
                await Task.Delay(50);
                i = v.Length;
            });
            Assert.Equal(0, i);
            task.Wait();
            Assert.Equal(3, i);
        }

        [Fact]
        public void UsingAsync_for_null_action_should_do_nothing()
        {
            var s = Option.From("foo");
            var task = s.DoWhenSomeAsync(null);
            task.Wait();
            // Test ok as function did not throw and value is same.
            Assert.Equal("foo", s.GetValueOrDefault());
        }
    }
}