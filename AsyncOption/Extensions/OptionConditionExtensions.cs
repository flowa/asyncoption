﻿using System;
using System.Runtime.CompilerServices;

namespace AsyncOption
{
    public static class OptionWhereExtensions {

        public static Option<T> Where<T>(
            this Option<T> option,
            Func<T, bool> predicate)
        {
            // GetValueOrDefault may throw e.g. if Enum does not have value for 0
            // thus check the value after IsSome check
            if (option.IsSome && predicate(option.GetValueOrDefault()))
            {
                return option;
            }
            // otherwise return None
            return Option.NoneOf<T>();
        }
    }
}