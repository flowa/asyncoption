using System;
using System.Threading.Tasks;

namespace AsyncOption
{
    public static class TaskExtensions {

        public static Option<T> AwaitAndContinue<T>(this Task<Option<T>> taskOption)
        {
            taskOption.Wait();
            return taskOption.Result;
        }

        public static async Task<Option<TResult>> MapAsync<T, TResult>(
            this Task<Option<T>> taskOption,
            Func<T, TResult> map
            )
        {
            var value = await taskOption;
            if (value.IsSome && map != null)
            {
                return map(value.GetValueOrDefault());
            }
            return Option<TResult>.AsNone();
        }

        // This and the one above are same becuase of type conversion!
        public static async Task<Option<TResult>> MapAsync<T, TResult>(
            this Task<Option<T>> taskOption,
            Func<T, Option<TResult>> map
            )
        {
            var value = await taskOption;
            if (value.IsSome && map != null)
            {
                return map(value.GetValueOrDefault());
            }
            return Option<TResult>.AsNone();
        }

        public static async Task<Option<TResult>> MapAsync<T, TResult>(
            this Task<Option<T>> taskOption,
            Func<T, Task<TResult>> map
            )
        {
            var value = await taskOption;
            if (value.IsSome && map != null)
            {
                return await map(value.GetValueOrDefault());
            }
            return Option<TResult>.AsNone();
        }

        // This and the one above are same becuase of type conversion!
        public static async Task<Option<TResult>> MapAsync<T, TResult>(
            this Task<Option<T>> taskOption,
            Func<T, Task<Option<TResult>>> map
            )
        {
            var value = await taskOption;
            if (value.IsSome && map != null)
            {
                return await map(value.GetValueOrDefault());
            }
            return Option<TResult>.AsNone();
        }


    }
}