using System;

namespace AsyncOption.InternalExtensions
{
    public static class OptionAutoMapper
    {
        public static bool ShouldAutoMapSome<T, TAny>(this Option<T> option, Func<T, TAny> mapSome)
            => (option.IsSome && mapSome == null);

        public static bool ShouldAutoMapNone<T, TAny>(this Option<T> option, Func<TAny> mapSome)
            => (option.IsSome && mapSome == null);

        public static bool ShouldMapSome<T, TAny>(this Option<T> option, Func<T, TAny> mapSome)
            => (option.IsSome && mapSome != null);

        public static bool ShouldMapNone<T, TAny>(this Option<T> option, Func<TAny> mapNone)
            => (option.IsNone && mapNone != null);

        public static Option<TResult> AutoMapNone<T, TResult>(this Option<T> option)
        {
            // None can be mapped to None regardless of the type.
            if (option.IsNone)
            {
                return Option<TResult>.AsNone();
            }
            throw new Exception("Expected Some. Option was not None.");
        }

        public static Option<TResult> AutoMapSome<T, TResult>(this Option<T> option)
        {
            ThrowIfNotTInputIsNotAssignableToTOutput<T, TResult>(
                "Cannot auto map Option. TResult must be assignable from T");
            // None can be mapped to None regardless of the type.
            if (option.IsSome)
            {
                // if this is not allowed method should throw.
                var mapped = (TResult)(option.GetValueOrDefault() as object);
                return Option<TResult>.From(mapped);
            }
            throw new Exception("Expected Some. Option was not Some.");
        }

        private static void ThrowIfNotTInputIsNotAssignableToTOutput<TInput, TOutput>(string error)
        {
            if (!typeof(TOutput).IsAssignableFrom(typeof(TInput)))
            {
                throw new Exception(error);
            }
        }
    }
}