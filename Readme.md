**AsyncOption is Option + (fluent version of) C#'s async-await.** 

In practice, AsyncOption is:

1. **Convenient and safe way to handle missing values** by using Option design pattern. 
2. **Modular way to write chained asynchronous calls in C#**.

What is `Option<T>`
=====================

Option<T> is a generic type that can be either Some or None. If it is Some it has non-null value T. 
If it is None it doesn't have value.

E.g. `Option<int>` is either `Option<int>.Some` or `Option<int>.None`. If `Option<int>` is `Some` 
is has value. It it does not have value, `Option<int>`is `None`. 
`Nullable<int>` use null as representation of missing value. `Option<int>.None` is not null.

(Type syntax might look here slightly weird, because CLR doesn't have native support for sum types. This 
implementation is similar to the implementation of F#'s discriminate union: I.e. Option<T>.Some and 
Option<T>.None both inherits abstract class Option<T>. They both are nested classes for Option<T>. 
Type name looks a bit weird but its rather convenient to use in practice.)

Installation
===============

[TODO]

How to use AsyncOption
========================

Create a Option<T> and convert it back to T
--------------------------------------------

```
#!csharp

// Create Option<string> 
var someString = Option.From("foo");

// Also implicit type case works
Option<string> someString2 = "string";

// Get value from the Option<string> 
var str = someString.GetValueOrDefault();

// Or you can also user explicit type cast
var str2 = (string) someString;
```

Initialize and use Option<T>.None
---------------------------------

Create new option as that does not have value (i.e. Option<T> is None):

```
#!csharp

var noneString = Option.NoneOf<string>()
```

You can call GetValueOrDefault of Option<T>.None and it does not throw. You can pass default value as an optional parameter. 
In more idiomatic implementations of value pattern this method is named ValueOr.

If Option is None and you don't pass defaultValue to function it returns default(T). For most types default(T) is null. 
If numbers such as int it is 0, and for bool it is false etc. Notice that for Enums default(T) may throw Exception if 
there is no value for 0 in Enum.

```
#!csharp

var noneString = Option.NoneOf<string>()

var nullStr    = noneString.GetValueOrDefault()       // srt will be null
var nonNullStr = noneString.GetValueOrDefault("none") // srt will be "none"
```

4.4 Getting started with synchronous Option<T>
---------------------------------------------

Following example illustrates asynchronous `Option<T>`s work. **You should not use `Option<T>` in this way!**. 

```
#!csharp

// These counters are used to illustrate that the calls are really asynchronous
var called1 = 0;
var called2 = 0;

// This is how you can build a asyncronous workflow having two async operations.
var task1 = Option<int>
    .From(0)
    .MapAsync(async (v) =>
    {
        await Task.Delay(100);
        called1++;
        return v + 2;
    }).MapAsync(async (v) =>
    {
        await Task.Delay(100);
        called2++;
        return v * 2;
    });

// Async Options is CRL Task<Option<T> you can with it anything you can do with tasks.
var awaiter = task1.GetAwaiter();

// At this point counters should not be called yet.
Assert.Equal(0, called1);
Assert.Equal(0, called2);

// After 150 ms first async lamnda should have been returned, but second have not.
Thread.Sleep(150);
Assert.Equal(1, called1);
Assert.Equal(0, called2);

// After 250 ms both async lamdas should have been returned
Thread.Sleep(100);
Assert.Equal(1, called1);
Assert.Equal(1, called2);

var result = awaiter.GetResult();
Assert.Equal(4, result.TryGetValue());
```

In most cases, you want to utilize call `AwaitAndContinue()`. It an extension method for `Task<Option<T>>` that reaturs Option<T> 
once asynchronous work is done:

```
#!csharp

var task1 = Option<int>
    .From(0)
    .MapAsync(async (v) =>
    {
        await Task.Delay(100);
        called1++;
        return v + 2;
    })

var optionValue = task1.AwaitAndContinue().GetValueOrDefault(); // AwaitAndContinue will block thread and eventually return 2 
```


Alternative approaches and implementations
=============================================

There are many other ways to solve the same problems AsyncOption solves. Because AsyncOption is opionated in many ways, its good 
to compare it with its alternatives. 

AsyncOption vs. Null propagation
--------------------------------

C# 6 introduced Null propagation as way to safely calls methods of objects that might be null. 

For instance, this does not throw error but return null:

```
#!csharp

Object o = null;
return o?.ToString(); // returns null;
```

The code above is a lot cleaner and more beautiful than this:

```
#!csharp
DateTime d = null;
return s == null ? null : s.ToString();
```

...or this:

```
#!csharp

DateTime d  = null;
if(d == null){
   return null;
}
return d.ToString();
```

Null propagation is very nice way minimize pain with nulls in simple cases. However, if your software have more than 
one method, its not sufficient solution to the problems nulls and missing values may cause.

Basically, you never really know outside a method, if caller has taken care of nulls or not. It would be a lot easier, 
if the code was never modified after it was written. Then you could, in fact, ensure that a value passed to a method 
is never null. In real life, this is not the case. The codebase have tendency to grow and mutate in a way you cannot
predict when you write a method. Thus, you each method should validate it's input. 

That's nice ideal, but it has price. In practice, you have to do a lot of redundant null checks. Eventually, big 
portion of code is just null checks and null related error handling. That is waste of time, but debugging code because 
of a missing null checks is even more wasteful. 

To make long story short, no matter if you do null checks or not, you are wasting time and adding incidental complexity 
to the software. 

Unlearning null check pattern
-----------------------------

You can use Option<T> `IsSome` and `IsNone` methdos to check if option is None or Some. 
Generally speaking avoid using IsSome and IsNone unless you cannot just Map values 
instead.

I.e. **DON'T** null check in disguise:

```
#!csharp
var optionX = Option.NoneOf<int>();

if(optionX.IsNone){
   optionX = Option.From(this.DefaultValue)
}
```

Rather use Map or FlatMap instead:

```
#!csharp
var optionX = Option.NoneOf<int>();

optionX = optionX.FlatMap(mapNone: () => 0);
```

Option<T> vs. Nullable<T>
--------------------------

Null *was* a big innovation and its a rather nice way to solved many problems. Even in .Net not all things can be 
null and that caused a lot of problems. 

Value types (int, long, bool, Enums, etc) had non-null default value. E.g. for number types it was 0, and for 
bool false and so on. However, sometimes the value is just missing, and it's crucial to know that a variable has
missing value. If that was the case, you had to write manual logic to signal whether a value exists or not. 

.Net 3.0 introduced `Nullable<T>` types as an answer to the problem. You simply wrapped a value type in an object 
that could be null. Type coercion allowed you to use nullable versions of types just alike non-nullable ones, 
but not always.

In addition, now you had to handle missing values differently with reference types and with value types. `Nullable<T>`
saved a lot time, but it didn't solve the problem. Even, if it isn't hard once you learn, how to use `Nullable<T>`, 
it significantly increase complexity of code. Option pattern allows you to handle **all** missing values in exactly 
same way. That is enormous improvement. 

The main difference with option pattern and Nullable<T> types is that when you work with Options, you want to keep 
data inside an Option type as long as posissble and take value out of the Option<T> when you really need it. This 
is easier to understand via and example.

In following example compare Nullable<T> and Option<T>:

```
#!csharp

int? nullableInt = null;

// This is a rather common type of a bug.
Console.Write("nullableInt " + nullableInt.ToString())

// At this point convert 3 to null, because 3 is used to indicate 
// missing value. This example is inspired by a real world example. 
// This doesn't decrease the WTF-score of this kind of logic.
if(nullableInt.HasValue && nullableInt == 3){
   nullableInt = nullableInt + 1;
}

return nullableInt.HasValue 
	? valueAsString = $"Value = {nullableInt.Value}"
	: valueAsString = "Value = no value";

```

The same when using Option<T>:

```
#!csharp

var optionInt = Option.NoneOf<int>();

// Option<T> doesn't allow you to get NullReferenceException.
optionInt.WhenSome((value) => Console.Write("nullableInt " + value.ToString()))

// You can use mapNone parameter to map None to Some. The first maps some to something else.
// Use FlatMap to map a value of Some to another value of Some. Instead of FlatMap you can use
// Map<T> but then you must specify T explicitly.
optionInt = optionInt.FlatMap(value => value + 1)

// Where returns null if the condition is not true. If it is true option is returned as is.
optionInt = optionInt.Where(value => value != 3);

// You you can map from Option<int> to Option<string>. You can also map None to Some of new type. 
// If mapping of None is not handled explicitly 'Option<T>.None' is automatically mapped to 
// 'Option<T>.None'
return noneInt.FlatMap(
   mapSome: (value) => $"Value = {value}";
   mapNone: () => "Value = no value";
)
```

4.3 AsyncOption vs. "plain" async-await
---------------------------------------

C# 5.0 introduced nice way to write asynchronous code by using `async` and `await` keywords. While those 
keywords made asyncronous programming a lot easier, this convenience had high price. The main problem 
is that async calls are very different from synchronous calls. As a consequence many APIs have two version 
of every single public methods: DoStuff and DoStuffAsync.

At some point you have to exit asynchronous world, but its not obvious when and where you should do so. 
Performance wise its the ideal is to do stuff syncronously as late as possible. However this makes 
testing and development hard and cumbersome.

AsyncOption allows you to "pipe" or "chain" use asynchronous computations. I practice this means that you 
can build a lazy workflows pretty easily from small composable pieces. The idea is a bit similar to function 
composition. C# does nos support function composition unlike more advanced and robust languages such as F#.

AsyncOption vs. Other implementation of Option pattern
-------------------------------------------------------

There are many other implementations to Option<T> pattern for C#. See e.g. 

* https://github.com/nlkl/Optional
* https://github.com/tejacques/Option

Option and Optional are both more idiomatic implementations of Option pattern. The key differences are:

* AsyncOption have helpers to Map and work with asynchronoys Options.
* AsyncOption allows you to implicitly cast T to Option<T> and Option<T> to T. As a consequence you Instead of ValueOr 
pattern for getting a value, GetValueOrDefault is used. None of T is casted to default value of T.
* AsyncOption contains set of helpers does not relate to Option pattern. For instance you can safely "use" value in option 
without mapping it by using DoWhenNone or DoWhenSome extension methods or their async counterparts.

## License

MIT