using System;
using System.Threading.Tasks;
using Xunit;

namespace AsyncOption.Test
{
    public class OptionMappingTest
    {
        public static object[] 
            DefTest<TOriginal, TMapped>(
            string name,
            Option<TOriginal>                        original, 
            Func<Option<TOriginal>, Option<TMapped>> function,
            Option<TMapped>                          expected = null)
        {
            return new object[]
            {
                name,
                original,
                function,
                expected
            };
        }

        public static object[][] MapFunctionsToTest =
            {
                DefTest<string, string>(
                    "Non-null mapper (FlatMap via type coarsion with explicit type) should not map None",
                    original: Option.NoneOf<string>(),
                    function: original => original.Map<string>((value) => value + value),
                    expected: Option.NoneOf<string>()
                ),
                DefTest<string, string>(
                    "Non-null mapper (FlatMap via type coarsionwith explicit type) should map Some object",
                    original: Option.From("foo"),
                    function: original => original.Map<string>((value) => value + value),
                    expected: Option.From("foofoo")
                ),
                DefTest<string, string>(
                    "Non-null mapper (FlatMap via type coarsion with explicit type) should not map Some object",
                    original: Option.From("foo"),
                    function: original => original.Map<string>((value) => value + value),
                    expected: Option.From("foofoo")
                ),
                DefTest<string, string>(
                    "Null mapper (FlatMap) should return Some object as is if types are same",
                    original: Option.From("foo"),
                    function: original => original.FlatMap<string>(null),
                    expected: Option.From("foo")
                ),
                DefTest<string, string>(
                    "Null mapper (FlatMap) should return None object as is",
                    original: Option.NoneOf<string>(),
                    function: original => original.FlatMap<string>(null),
                    expected: Option.NoneOf<string>()
                ),
                DefTest<string, string>(
                    "Non-null mapper (FlatMap) should not map None object",
                    original: Option.NoneOf<string>(),
                    function: original => original.FlatMap((value) => value + value),
                    expected: Option.NoneOf<string>()
                ),
                DefTest<string, string>(
                    "Non-null mapper (FlatMap) should not map Some object",
                    original: Option.From("foo"),
                    function: original => original.FlatMap((value) => value + value),
                    expected: Option.From("foofoo")
                ),
                DefTest<string, string>(
                    "Null mapper (Map with explicit type) should return Some object as is, if types are same",
                    original: Option.From("foo"),
                    function: original => original.Map<string>(null),
                    expected: Option.From("foo")
                ),
                DefTest<string, string>(
                    "Null mapper (Map with explicit type) should return None object as is",
                    original: Option.NoneOf<string>(),
                    function: original => original.Map<string>(null),
                    expected: Option.NoneOf<string>()
                ),
                DefTest<string, string>(
                    "Non-null mapper (Map) should not map None",
                    original: Option.NoneOf<string>(),
                    function: original => original.FlatMap((value) => value + value),
                    expected: Option.NoneOf<string>()
                ),
                DefTest<string, string>(
                    "Non-null mapper (Map) should not map Some object",
                    original: Option.From("foo"),
                    function: original => original.FlatMap((value) => value + value),
                    expected: Option.From("foofoo")
                )
            };

        [Theory, MemberData(nameof(MapFunctionsToTest))]
        public void Test_Map_when_mapping_to_same_type(
            string name,
            Option<string> original, 
            Func<Option<string>, Option<string>> testFunction ,
            Option<string> expected
            )
        {
            var actual = testFunction(original);
            Assert.Equal(expected.IsSome, actual.IsSome);
            Assert.Equal(expected.IsNone, actual.IsNone);
            Assert.Equal(
                expected.GetValueOrDefault(), actual.GetValueOrDefault());
        }

        public static object[][] Map_T1_To_T2_FunctionsToTest =
        {
                DefTest<string, int>(
                    "Non-null mapper (FlatMap via type coarsion with explicit type) should not map None",
                    original: Option.NoneOf<string>(),
                    function: original => original.Map<int>((value) =>  value.Length),
                    expected: Option.NoneOf<int>()
                ),
                DefTest<string, int>(
                    "Non-null mapper (FlatMap via type coarsionwith explicit type) should map Some object",
                    original: Option.From("foo"),
                    function: original => original.Map<int>((value) => value.Length),
                    expected: Option.From(3)
                ),
                DefTest<string, int>(
                    "Null mapper (FlatMap) should return None object as is",
                    original: Option.NoneOf<string>(),
                    function: original => original.FlatMap<int>(null),
                    expected: Option.NoneOf<int>()
                ),
                DefTest<string, int>(
                    "Non-null mapper (FlatMap) should not map Some object",
                    original: Option.From("foo"),
                    function: original => original.FlatMap((value) => value.Length),
                    expected:  Option.From(3)
                ),
                DefTest<string, int>(
                    "Null mapper (Map with explicit type) should return None object as is",
                    original: Option.NoneOf<string>(),
                    function: original => original.Map<int>(null),
                    expected: Option.NoneOf<int>()
                ),
                DefTest<string, int>(
                    "Non-null mapper (Map) should not map None; Yet it defines the type of none.",
                    original: Option.NoneOf<string>(),
                    function: original => original.FlatMap((value) => value.Length),
                    expected: Option.NoneOf<int>()
                ),
                DefTest<string, int>(
                    "Non-null mapper (Map) should map Some object",
                    original: Option.From("foo"),
                    function: original => original.FlatMap((value) => value.Length),
                    expected: Option.From(3)
                )
            };

        [Theory, MemberData(nameof(Map_T1_To_T2_FunctionsToTest))]
        public void Test_Map_when_mapping_to_different_types(
                string name,
                Option<string> original,
                Func<Option<string>, Option<int>> testFunction,
                Option<int> expected
                )
        {
            var actual = testFunction(original);
            Assert.Equal(expected.IsSome, actual.IsSome);
            Assert.Equal(expected.IsNone, actual.IsNone);
            Assert.Equal(
                expected.GetValueOrDefault(), actual.GetValueOrDefault());
        }


        [Fact]
        public void FlatMap_with_null_function_should_throw_if_automapping_is_not_possible()
        {
            var original = Option.From("foo");
            Assert.Throws<Exception>(() => { original.Map<int>(null); });
        }

        [Fact]
        public void FlatMap_For_None_is_returns_None_and_does_not_invove_mapSome_function()
        {
            var original = Option.From<string>(null);
            var mapped = original.Map<int>((v) => { throw new Exception("This should not happen"); });
            Assert.True(original.IsNone);
            Assert.True(mapped.IsNone);
        }

        [Fact]
        public void FlatMap_for_Some_maps_value_to_Option_of_new_type()
        {
            var original = Option.From("foo");
            var mapped = original.FlatMap((v) => v.Length);
            Assert.True(mapped.IsSome);
            Assert.False(original.IsNone);
            Assert.IsAssignableFrom<Option<int>.Some>(mapped);
            Assert.Equal(3, mapped.GetValueOrDefault());
        }

        [Fact]
        public void Map_with_null_function_should_return_the_original_option_as_is()
        {
            var original = Option.From("foo");
            var mapped = original.Map<string>(null);
            Assert.True(mapped.IsSome);
            Assert.Same(original.GetValueOrDefault(), mapped.GetValueOrDefault());
        }

        [Fact]
        public void Map_with_null_function_should_throw_if_automapping_is_not_possible()
        {
            var original = Option.From("foo");
            Assert.Throws<Exception>(
                () => { original.Map<int>(null); }
                );
        }

        [Fact]
        public void Map_For_None_is_returns_None_and_does_not_invove_mapSome_function()
        {
            var original = Option.NoneOf<string>();
            var mapped = original.Map<string>((v) => { throw new Exception("This should not happen"); });
            Assert.True(original.IsNone);
            Assert.True(mapped.IsNone);
        }

        [Fact]
        public void Map_for_Some_maps_value_to_Option_of_new_type()
        {
            var original = Option.From("foo");
            var mapped = original.FlatMap((v) => v.Length);
            Assert.True(mapped.IsSome);
            Assert.False(original.IsNone);
            Assert.IsAssignableFrom<Option<int>.Some>(mapped);
            Assert.Equal(3, mapped.GetValueOrDefault());
        }


    }

    public class OptionAsyncMappingTest
    {

        [Fact]
        public void MapAsync_with_null_function_should_return_the_same_option()
        {
            var original = Option.From("foo");
            var mappedTask = original.MapAsync<string>(null);
            mappedTask.Wait();
            Assert.True(mappedTask.Result.IsSome);
            Assert.Equal("foo", mappedTask.Result.GetValueOrDefault());
        }

        [Fact]
        public void MapAsync_for_None_is_should_return_None_and_is_allways_safe()
        {
            var s = Option.NoneOf<string>();
            var task = s.MapAsync<int>(async (v) => { throw new Exception("This should not happen"); });
            task.Wait();
            Assert.True(task.Result.IsNone);
        }

        [Fact]
        public void MapAsync_for_Some_should_map_value_to_Option_of_new_type()
        {
            var s = Option.From("foo");
            var task = s.MapAsync(async (v) => await Task.FromResult(Option.From(v.Length)));
            task.Wait();

            Assert.True(task.Result.IsSome);
            Assert.IsAssignableFrom<Option<int>.Some>(task.Result);
            Assert.Equal(3, task.Result.GetValueOrDefault());
        }
    }
}