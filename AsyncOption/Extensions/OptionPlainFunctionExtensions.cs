﻿using System;

namespace AsyncOption
{
    public static class OptionPlainFunctionExtensions
    {
        public static Option<TResult> Map<TResult, T>(
            Option<T> originalOption,
            Func<T, TResult> mapSome,
            Func<Option<TResult>> mapNone = null
            )
        {
            return originalOption.Map(
                (some) =>
                {
                    var mappedValue = mapSome(originalOption.GetValueOrDefault());
                    return new Option<TResult>.Some(mappedValue);
                }, mapNone);
        }
    }
}