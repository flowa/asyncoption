using System;
using System.Threading.Tasks;
using AsyncOption.InternalExtensions;

namespace AsyncOption
{
    public static class OptionAsyncMapper
    {
        /// <summary>
        /// Enter to async processing.
        /// </summary>
        /// <typeparam name="T">Type of original option</typeparam>
        /// <typeparam name="TResult">Balue of </typeparam>
        /// <param name="original">Original option</param>
        /// <param name="mapSome">Asyncronous mapping from T to Option of TResult</param>
        /// <param name="mapNone">Asyncronous mapping from nothing to Option of TResult</param>
        /// <returns>Task that wraps Option of TResult</returns>
        public static async Task<Option<TResult>> MapAsync<T, TResult>(
            Option<T> original,
            Func<T, Task<Option<TResult>>> mapSome,
            Func<Task<Option<TResult>>> mapNone
            )
        {
            // If no mapping matches return None
            Option<TResult> mappedOption = Option.NoneOf<TResult>();
            if (original.ShouldMapNone(mapNone))
            {
                mappedOption = await mapNone();
            }

            if (original.ShouldAutoMapSome(mapSome))
            {
                mappedOption = original.AutoMapSome<T, TResult>();
            }

            if (original.ShouldMapSome(mapSome))
            {
                var value = original.GetValueOrDefault();
                mappedOption = await mapSome(value);
            }

            return await Task.FromResult(mappedOption);
        }

        public static async Task<Option<TResult>> FlatMapAsync<T, TResult>(
            Option<T> original,
            Func<T, Task<TResult>> mapSome,
            Func<Task<TResult>> mapNone)
        {
            // If no mapping matches return None
            Option<TResult> mappedOption = Option.NoneOf<TResult>();
            if (original.ShouldMapNone(mapNone))
            {
                mappedOption = await mapNone();
            }

            if (original.ShouldAutoMapSome(mapSome))
            {
                mappedOption = original.AutoMapSome<T, TResult>();
            }

            if (original.ShouldMapSome(mapSome))
            {
                var value = original.GetValueOrDefault();
                mappedOption = await mapSome(value);
            }

            return await Task.FromResult(mappedOption);

        }


    }
}