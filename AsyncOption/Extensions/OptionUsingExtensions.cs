﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AsyncOption
{
    public static class OptionWhenExtensions
    {
        public static Option<T> DoWhenSome<T>(this Option<T> option, Action<T> action)
        {
            if (option.IsSome && action != null)
            {
                action(option.GetValueOrDefault());
            }
            return option;
        }

        public static Option<T> DoWhenNone<T>(this Option<T> option, Action action)
        {
            if (option.IsNone && action != null)
            {
                action();
            }
            return option;
        }

        public static async Task<Option<T>> DoWhenSomeAsync<T>(this Option<T> option, Func<T, Task> action)
        {
            if (option.IsSome && action != null)
            {
                await action(option.GetValueOrDefault());
            }
            return await Task.FromResult(option);
        }

        public static async Task<Option<T>> DoWhenSomeNone<T>(this Option<T> option, Func<T, Task> action)
        {
            if (option.IsSome && action != null)
            {
                await action(option.GetValueOrDefault());
            }
            return await Task.FromResult(option);
        }
    }
}
