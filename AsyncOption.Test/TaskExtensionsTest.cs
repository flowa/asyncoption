using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace AsyncOption.Test
{
    public class TaskExtensionsTest
    {
        public async Task<int> Len(string str)
        {
            await Task.Delay(100);
            return await Task.FromResult(str.Length);
        }

        [Fact]
        public void MapAsync_call_should_be_asyncronous_simple()
        {
            var called1 = 0;

            var task1 = Option.From("foo")
                .FlatMapAsync(async (str) =>
                {
                    var value = await Len(str);
                    called1++;
                    return value;
                });
            Assert.Equal(0, called1);
            var value1 = task1.AwaitAndContinue().GetValueOrDefault();
            Assert.Equal(3, value1);
        }

        [Fact]
        public void MapAsync_call_should_be_asyncronous_non_async_chaining()
        {
            var called1 = 0;
            var called2 = 0;
            var task1 = Option.From("foo")
                .FlatMapAsync(async (str) =>
                {
                    var value = await Len(str);
                    called1++;
                    return value;
                }).MapAsync((i) =>
                {
                    called2++;
                    return i + 1;
                });
            Assert.Equal(0, called1);
            Assert.Equal(0, called2);

            var value1 = task1.AwaitAndContinue().GetValueOrDefault();
            Assert.Equal(1, called1);
            Assert.Equal(1, called2);
            Assert.Equal(4, value1);
        }

        [Fact]
        public void MapAsync_call_should_be_asyncronous_when_using_async_chaining()
        {
            var called1 = 0;
            var called2 = 0;
            var task1 = Option.From(0)
                .FlatMapAsync(async (v) =>
                {
                    await Task.Delay(100);
                    called1++;
                    return v + 2;
                }).MapAsync(async (v) =>
                {
                    await Task.Delay(100);
                    called2++;
                    return v * 2;
                });
            Assert.Equal(0, called1);
            Assert.Equal(0, called2);

            var a = task1.GetAwaiter();
            Assert.Equal(0, called1);
            Assert.Equal(0, called2);

            // After 150 ms first async should be called, but second is not.
            Thread.Sleep(150);
            Assert.Equal(1, called1);
            Assert.Equal(0, called2);

            // After 300 ms both asyncs should be called
            Thread.Sleep(150);
            Assert.Equal(1, called1);
            Assert.Equal(1, called2);

            var result = a.GetResult();
            Assert.Equal(4, result.GetValueOrDefault());
        }

        [Fact]
        public void MapAsync_call_should_be_do_call_only_if_value_is_Some()
        {
            var called1 = 0;
            var called2 = 0;
            var task1 = Option.From("foo")
                .MapAsync(async (v) =>
                {
                    await Task.Delay(200);
                    called1++;
                    return Option.NoneOf<string>();
                }).MapAsync(async (v) =>
                {
                    await Task.Delay(200);
                    called2++;
                    return "It should never return this.";
                });
            Assert.Equal(0, called1);
            Assert.Equal(0, called2);

            var value = task1.AwaitAndContinue();
            Assert.Equal(1, called1);
            Assert.Equal(0, called2);
            Assert.True(value.IsNone);
        }


    }
}