using System;
using AsyncOption.InternalExtensions;

namespace AsyncOption
{
    public static class OptionMapper
    {
        public static Option<TResult> Map<T, TResult>(
                            Option<T> original,
                            Func<T, Option<TResult>> mapSome,
                            Func<Option<TResult>> mapNone)
        {
            // If no mapping matches return None
            Option<TResult> mappedOption = Option.NoneOf<TResult>();
            if (original.ShouldMapNone(mapNone))
            {
                mappedOption = mapNone();
            }

            if (original.ShouldAutoMapSome(mapSome))
            {
                mappedOption = original.AutoMapSome<T, TResult>();
            }

            if (original.ShouldMapSome(mapSome))
            {
                var value = original.GetValueOrDefault();
                mappedOption = mapSome(value);
            }

            return mappedOption;
       }

        public static Option<TResult> FlatMap<T, TResult>(
                            Option<T> original,
                            Func<T, TResult> mapSome,
                            Func<TResult> mapNone)
        {
            // If no mapping matches return None
            Option<TResult> mappedOption = Option.NoneOf<TResult>();
            if (original.ShouldMapNone(mapNone))
            {
                mappedOption = mapNone();
            }

            if (original.ShouldAutoMapSome(mapSome))
            {
                mappedOption = original.AutoMapSome<T, TResult>();
            }

            if (original.ShouldMapSome(mapSome))
            {
                var value = original.GetValueOrDefault();
                mappedOption = mapSome(value);
            }
            return mappedOption;
        }

    }
}